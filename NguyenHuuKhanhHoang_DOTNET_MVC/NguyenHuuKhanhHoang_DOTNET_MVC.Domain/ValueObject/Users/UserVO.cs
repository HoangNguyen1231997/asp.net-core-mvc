﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NguyenHuuKhanhHoang_DOTNET_MVC.Core.ValueObject.Users
{
    public class UserVO
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public List<UserRoleVO> UserRoles { get; set; }
    }
}
