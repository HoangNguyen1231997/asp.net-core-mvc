﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NguyenHuuKhanhHoang_DOTNET_MVC.Core.ValueObject.Users
{
    public class RoleVO
    {
        public long Id { get; set; }
        public string RoleName { get; set; }
        public List<UserRoleVO> UserRoles { get; set; }
    }
}
