﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NguyenHuuKhanhHoang_DOTNET_MVC.Core.ValueObject.Users
{
    public class UserRoleVO
    {
        public long Id { get; set; }
        public long RoleId { get; set; }
        public long UserId { get; set; }
        public virtual RoleVO Role { get; set; }
        public virtual UserVO User { get; set; }
    }
}
