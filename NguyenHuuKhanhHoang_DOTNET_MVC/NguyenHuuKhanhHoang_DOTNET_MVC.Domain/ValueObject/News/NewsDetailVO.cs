﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NguyenHuuKhanhHoang_DOTNET_MVC.Core.ValueObject.News
{
    public class NewsDetailVO
    {
        public long Id { get; set; }
        public long NewsId { get; set; }
        public string NoiDung { get; set; }
        public string NoiDungSub { get; set; }
        public string ThoiGianDang { get; set; }
        public string ImageUrl { get; set; }
        public string ImageUrlSub { get; set; }
    }
}
