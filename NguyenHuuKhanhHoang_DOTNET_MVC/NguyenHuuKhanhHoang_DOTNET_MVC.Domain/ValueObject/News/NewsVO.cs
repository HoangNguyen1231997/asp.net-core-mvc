﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NguyenHuuKhanhHoang_DOTNET_MVC.Core.ValueObject.News
{
    public class NewsVO
    {
        public long Id { get; set; }
        public string TieuDe { get; set; }
        public string TheLoai { get; set; }
        public int Type { get; set; }
        public NewsDetailVO newsDetail { get; set; }
    }
}
