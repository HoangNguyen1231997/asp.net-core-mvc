﻿using NguyenHuuKhanhHoang_DOTNET_MVC.Core.Domain.Entity.Comments;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace NguyenHuuKhanhHoang_DOTNET_MVC.Core.Domain.Entity.Users
{
    [Table("User")]
    public class User
    {
        [Key]
        public long Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string GioiThieu { get; set; }
        public ICollection<Core.Domain.Entity.News.News> News { get; set; }
        public ICollection<UserRole> UserRoles { get; set; }
    }
}
