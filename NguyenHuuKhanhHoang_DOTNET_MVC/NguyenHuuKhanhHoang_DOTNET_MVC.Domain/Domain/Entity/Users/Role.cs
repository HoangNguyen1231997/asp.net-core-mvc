﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace NguyenHuuKhanhHoang_DOTNET_MVC.Core.Domain.Entity.Users
{
    [Table("Role")]
    public class Role
    {
        [Key]
        public long Id { get; set; }
        [Required]
        public string RoleName { get; set; }
        public ICollection<UserRole> UserRoles { get; set; }
    }
}
