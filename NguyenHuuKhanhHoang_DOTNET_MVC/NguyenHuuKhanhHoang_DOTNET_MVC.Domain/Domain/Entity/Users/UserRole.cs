﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace NguyenHuuKhanhHoang_DOTNET_MVC.Core.Domain.Entity.Users
{
    [Table("UserRole")]
    public class UserRole
    {
        [Key]
        public long Id { get; set; }
        public long RoleId { get; set; }
        public long UserId { get; set; }
        [ForeignKey("RoleId")]
        public Role Role { get; set; }
        [ForeignKey("UserId")]
        public User User { get; set; }
    }
}
