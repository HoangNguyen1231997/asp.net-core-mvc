﻿using NguyenHuuKhanhHoang_DOTNET_MVC.Core.Domain.Entity.Comments;
using NguyenHuuKhanhHoang_DOTNET_MVC.Core.Domain.Entity.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace NguyenHuuKhanhHoang_DOTNET_MVC.Core.Domain.Entity.News
{
    [Table("News")]
    public class News
    {
        [Key]
        public long Id { get; set; }
        public long UserId { get; set; }
        [Required]
        public string TieuDe { get; set; }
        [Required]
        public string TheLoai { get; set; }
        [Required]
        public int Type { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        public virtual NewsDetail NewsDetail { get; set; }
        public ICollection<Comment> Comments { get; set; }
    }
}
