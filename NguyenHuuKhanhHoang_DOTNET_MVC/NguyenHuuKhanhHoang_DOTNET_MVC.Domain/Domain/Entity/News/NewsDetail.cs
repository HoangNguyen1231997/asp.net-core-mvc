﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace NguyenHuuKhanhHoang_DOTNET_MVC.Core.Domain.Entity.News
{
    [Table("NewsDetail")]
    public class NewsDetail
    {
        [Key]
        public long Id { get; set; }
        public long NewsId { get; set; }
        [Required]
        public string NoiDung { get; set; }
        public string NoiDungSub { get; set; }
        [Required]
        public string ThoiGianDang { get; set; }
        [Required]
        public string ImageUrl { get; set; }
        
        public string ImageUrlSub { get; set; }
        [ForeignKey("NewsId")]
        public virtual News News { get; set; }
    }
}
