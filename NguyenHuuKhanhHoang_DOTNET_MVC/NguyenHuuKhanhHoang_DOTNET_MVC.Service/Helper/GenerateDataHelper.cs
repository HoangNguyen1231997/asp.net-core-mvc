﻿using NguyenHuuKhanhHoang_DOTNET_MVC.Core.ValueObject.News;
using NguyenHuuKhanhHoang_DOTNET_MVC.Core.ValueObject.Users;

using System;
using System.Collections.Generic;
using System.Text;

namespace NguyenHuuKhanhHoang_DOTNET_MVC.Service.Helper
{
    public static class GenerateDataHelper
    {
        public static List<UserVO> DummyUser()
        {
            var lstUserRole = new List<UserRoleVO>();
            lstUserRole.Add(new UserRoleVO()
            {
                Id = 1,
                RoleId = 1,
                UserId = 1,
                
            });
            lstUserRole.Add(new UserRoleVO()
            {
                Id = 2,
                RoleId =2,
                UserId = 2
            });
            var lstRole = new List<RoleVO>();
            lstRole.Add(new RoleVO()
            {
                Id = 1,
                RoleName = "Admin",
                UserRoles = new List<UserRoleVO>() { lstUserRole[0]}
               
            });
            lstRole.Add(new RoleVO()
            {
                Id = 2,
                RoleName = "User",
                UserRoles = new List<UserRoleVO>() { lstUserRole[1] }

            });
            lstUserRole[0].Role = lstRole[0];
            lstUserRole[1].Role = lstRole[1];
            var lstUser = new List<UserVO>();
            lstUser.Add(new UserVO()
            { 
                Id=1,
                Name = "Admin",
                Password = "123",
                UserRoles = new List<UserRoleVO>() { lstUserRole[0] }

            });
            lstUser.Add(new UserVO()
            {
                Id = 1,
                Name = "User",
                Password = "456",
                UserRoles = new List<UserRoleVO>() { lstUserRole[1] }

            });
            return lstUser;
        }
        public static List<NewsVO> DummyData()
        {
            var news = new List<NewsVO>();
            news.Add(new NewsVO()
            {
                Id = 1,
                TheLoai = "Công nghệ thông tin",
                TieuDe = "NashTech announced as the largest IT Company in Vietnam (*)",
                Type=1,
                newsDetail = new NewsDetailVO()
                {
                    Id = 1,
                    ImageUrl = "https://www.nashtechglobal.com/wp-content/uploads/2018/11/Top-50-IT-company.jpg",
                    NewsId = 1,
                    ThoiGianDang = "September 19 2018",
                    NoiDung = "NashTech – part of Harvey Nash Group – has been awarded as the largest technology company in Vietnam in terms of revenue in the ITO, BPO and KPO sector in the official 2018 list of the ‘’ by the Vietnam Software and IT Services Association – VINASA. NashTech is also honored to be in the prestigious list of ‘<strong>Top 10 Outstanding Companies with Industry 4.0 Technology Capability</strong>’, launched for the first time in 2018." +
                    "This is the fifth consecutive year NashTech has been recognised as one of the ‘Top IT Companies’ since the launch of the awards in 2014. The list is selected by the Selection Committee, which comprises of experts who conduct assessments and evaluations and selection is based on rigorous criteria and international standards. Companies chosen have had strong development and could affect the development trend of the market and IT sector in Vietnam.",
                    ImageUrlSub = "http://nashtechglobal.com/wp-content/uploads/2018/11/DSC_6092.jpg",
                    NoiDungSub = "With more than 20 years offering software outsourcing services to clients around the world, NashTech has built up a great team of thousands of engineers that are capable to deliver software projects that work. Our service quality is qualified by industrial highest standards, CMMI5, ISO27001 and more. We have built up our solutions portfolio with successful projects in advanced technology such as Blockchain, AR/VR, Machine Learning, RPA, Big Data, IoT and more." +
                    "2018 sees the significant impact that NashTech’s R&D team brought to the business and our clients. Many of the team’s efforts during the last couple of years in researching and building the company’s capability in new technologies has started blooming. We are building technical solutions that are transforming the way many companies run their businesses. Solutions such as our development for smart parking as part of smart city solutions, applying Machine Learning and AI to facilitate learning in the journey of digital disruption in education, helping media companies build their product on the cloud to get real time results of their media campaign, establishing an ICO platform via our fully accredited blockchain solution for a financial services company in Japan, and so much more."

                }
            });
            news.Add(new NewsVO()
            {
                Id =2,
                TheLoai = "Công nghệ thông tin",
                TieuDe = "COVID-19: We’re ready to support you",
                Type=1,
                newsDetail = new NewsDetailVO()
                {
                    Id = 2,
                    ImageUrl = "https://www.nashtechglobal.com/wp-content/uploads/2020/03/Hands-Connecting-Featured-Image.jpg",
                    NewsId = 2,
                    ThoiGianDang = " March 19 2020",
                    NoiDung = "We have been preparing at the very start of when the virus appeared in China, testing our home working systems and practices, doing everything possible to keep our people safe and ensuring our online business doors are wide open and ready to operate from our homes around the world.",
                    ImageUrlSub = "https://static.ybox.vn/2019/6/2/1559626305620-27.png",
                    NoiDungSub = "We are here ready to support you and proactively reaching out to all our clients, candidates and contractors. Please do get in touch with us if you have any questions or need our help."

                }
            });
            news.Add(new NewsVO()
            {
                Id = 3,
                TheLoai = "Công nghệ thông tin",
                TieuDe = "NashTech Wins Two Awards at Prestigious Sao Khue 2019",
                Type = 1,
                newsDetail = new NewsDetailVO()
                {
                    Id =3,
                    ImageUrl = "https://www.nashtechglobal.com/wp-content/uploads/2019/04/2019-Sao-Khue-banner.jpg",
                    NewsId = 3,
                    ThoiGianDang = "April 23 2019",
                    NoiDung = "NashTech, part of Harvey Nash Group and leading software development company in Viet Nam, has been awarded the prestigious Sao Khue award in 2019 in the Software Outsourcing and Business Process Service (BPO) categories. Winning the prestigious award for its 11th consecutive year further strengthens NashTech’s brand as one of the leading outsourcing companies in Vietnam. "
                    +"NashTech’s Software Outsourcing division provides comprehensive solutions that enable clients to optimise their investment. Using an international development model, combined with the Vietnam-based technology development center, has helped clients reduce costs while ensuring international quality standards.",
                    ImageUrlSub = "http://www.nashtechglobal.com/wp-content/uploads/2019/04/NASH-TECH-300x200.jpg",
                    NoiDungSub = "By clearly understanding the needs of businesses, and through its own experience and knowledge, NashTech has built an effective BPO division, with strict quality management systems to reduce costs and increase value for clients." +
                    "NashTech continue their efforts to create sustainable values that contribute to the ICT industry in Viet Nam. In March, NashTech was ranked among the top four IT companies with the best working environment, according to Anphabe Vietnam’s annual survey. Winning such important awards motivates NashTech to further contribute to the Vietnam ICT industry for years to come."

                }
            });
            news.Add(new NewsVO()
            {
                Id = 4,
                TheLoai = "Công nghệ thông tin",
                TieuDe = "NashTech at the 2018 Asia House Asian Business Leaders Award Dinner",
                Type = 2,
                newsDetail = new NewsDetailVO()
                {
                    Id = 4,
                    ImageUrl = "https://www.nashtechglobal.com/wp-content/uploads/2018/11/Asia-house-business-leaders-award-dinner.jpg",
                    NewsId = 4,
                    ThoiGianDang = "November 27 2018",
                    NoiDung = "NashTech attended the Asian Business Leaders Award Dinner 2018 at the Fullerton Hotel, Singapore on Thursday 22nd November." +
                    "The Dinner brings together leading figures in global business and diplomacy to celebrate the important work Asia House does in driving economic engagement between Europe and Asia, as well as the wider social value that commercial enterprise creates.",
                    ImageUrlSub = "http://www.nashtechglobal.com/wp-content/uploads/2019/04/NASH-TECH-300x200.jpg",
                    NoiDungSub = "The event, sponsored by Harvey Nash, featured speeches from Dr Kai-Fu Lee – the global thought-leader in Artificial Intelligence (AI); Bicky Bhangu, President of Rolls-Royce Southeast Asia, Pacific and South Korea, and BBC World News presenter Sharanjit Ley"

                }
            });
            news.Add(new NewsVO()
            {
                Id = 5,
                TheLoai = "Công nghệ thông tin",
                TieuDe = "New visual identity to support NashTech’s growth",
                Type = 2,
                newsDetail = new NewsDetailVO()
                {
                    Id = 5,
                    ImageUrl = "https://www.nashtechglobal.com/wp-content/uploads/2019/12/Launch-of-Visual-Identity-banner.jpg",
                    NewsId = 5,
                    ThoiGianDang = "January 8 2020",
                    NoiDung = "We have some exciting news. As part of NashTech’s growth plans and to mark our 20" +
                    "We officially launched the NashTech new look and feel on 15th January 2020 at events across the globe, where thousands of NashTech employees came together to celebrate. It was a lot of fun!",
                    ImageUrlSub = "http://nashtechglobal.com/wp-content/uploads/2020/01/LinkedIn-shared-image.png",
                    NoiDungSub = "The logo is proudly distinct and gets this from the frame. It’s a frame for creativity, a frame for possibility, and conversely represents boundaries that we break in our ambition to deliver technology excellence." +
                    "We chose a minimal logo because to us it shows how NashTech boldly cuts through complexity. Having images interact with the frame shows that within any given framework, we are agile, and constantly moving forward."

                }
            });
            news.Add(new NewsVO()
            {
                Id = 6,
                TheLoai = "Công nghệ thông tin",
                TieuDe = "NashTech Platform Delivers Better Customer Experience for Minster Law Clients",
                Type = 2,
                newsDetail = new NewsDetailVO()
                {
                    Id = 6,
                    ImageUrl = "https://www.nashtechglobal.com/wp-content/uploads/2019/04/Minster-Law-Case-Study.jpg",
                    NewsId = 6,
                    ThoiGianDang = "April 25 2019",
                    NoiDung = "They say the test of any quality insurer comes down to how well they handle their clients’ claims. That’s why, for Minster Law, a leading personal-injury practice" +
                    "Push it, “Right at a technical level, there was a clear synergy between us and NashTech. We bought into a sophisticated solutions provider…not just a group of developers.”",
                    ImageUrlSub = "http://nashtechglobal.com/wp-content/uploads/2020/01/LinkedIn-shared-image.png",
                    NoiDungSub = "Lower than some of the other systems we’ve got,” states Harry Cooke, Development Manager at Minster Law.Benefiting from the flexibility of the platform over and abo" +
                    "Key to its success has been its user-friendly interface and reliability. “One of the key measures that you can take is the "

                }
            });
            news.Add(new NewsVO()
            {
                Id = 7,
                TheLoai = "Công nghệ thông tin",
                TieuDe = "2018 Harvey Nash & KPMG Survey Launches",
                Type = 2,
                newsDetail = new NewsDetailVO()
                {
                    Id =7,
                    ImageUrl = "https://www.nashtechglobal.com/wp-content/uploads/2018/06/Harvey-Nash-KPMG-CIO-Survey-2018.jpg",
                    NewsId = 7,
                    ThoiGianDang = "April 25 2019",
                    NoiDung = "The largest IT leadership survey in the world, analyzing responses from organizations with a combined annual cyber security spend of up to US$46bn , found almost a quarter (23 percent) more respondents than in 2017 are prioritizing improvements in cyber security as cyber crime threats reach an all-time high, while managing operational risk and compliance has also become a significantly increased priority (up 12 percent). These two areas represent the fastest growing IT priorities of company boards.",
                    ImageUrlSub = "https://www.nashtechglobal.com/wp-content/uploads/2018/06/Harvey-Nash-KPMG-CIO-Survey-2018.jpg",
                    NoiDungSub = "A move towards digital platforms and solutions is proving a huge challenge for CIOs. While organizations recognize an effective digital strategy is critical to successful data security, many report they still struggle – with 78 percent stating that their digital strategy is only moderately effective, or worse. More than a third of companies (35 percent) can’t hire and develop the people they need with digital skills. And almost one in ten (9 percent) think that there is no clear digital vision or strategy at all."

                }
            });
            news.Add(new NewsVO()
            {
                Id = 8,
                TheLoai = "Công nghệ thông tin",
                TieuDe = "10th consecutive year NashTech honored at Sao Khue Awards",
                Type = 2,
                newsDetail = new NewsDetailVO()
                {
                    Id = 8,
                    ImageUrl = "https://www.nashtechglobal.com/wp-content/uploads/2018/04/Saokhue-award-2018.jpg",
                    NewsId = 8,
                    ThoiGianDang = "April 25 2019",
                    NoiDung = "Since 2003, Sao Khue has been the most prestigious award in Viet Nam. The awards recognise ICT companies who offer products and services that prove to be the best solutions for both business and society, as well as having the highest economic efficiency and remarkable contribution to the development of the ICT industry in Viet Nam. Over the past 15 years, the Sao Khue Award has been constantly innovating in line with the strong development of the Viet Nam market and strives to stay on top of the IT transformation wave and trends.",
                    ImageUrlSub = "http://nashtechglobal.com/wp-content/uploads/2018/04/NashTech-1-300x200.jpg",
                    NoiDungSub = "Sao Khue Award is sponsored by Viet Nam Association for Software and IT Services (VINASA), a non-profit, non-profit organization representing Viet Nam ICT industry, honoring and praising enterprises, agencies, collectives and individuals who have made outstanding contributions to the development of Viet Nam’s software and information technology industry."

                }
            });
            return news;
        }
     }
    
}
