﻿using NguyenHuuKhanhHoang_DOTNET_MVC.Core.Domain.Entity.Users;
using NguyenHuuKhanhHoang_DOTNET_MVC.Core.ValueObject.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace NguyenHuuKhanhHoang_DOTNET_MVC.Service.Service.Users
{
    public interface IUserService
    {
        UserVO GetDeatilUser(string name, string pass);
        User GetDeatilUserLogin(string name, string pass);
        User CheckUserExist(string name);
        void AddNewUser(User userEntity);
        void AddNewRole(UserRole userRoleEntity);
        string PasswordHash(string password);
    }
}
