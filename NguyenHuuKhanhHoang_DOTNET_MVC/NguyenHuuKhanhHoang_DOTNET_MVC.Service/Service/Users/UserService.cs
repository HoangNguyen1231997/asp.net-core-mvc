﻿using NguyenHuuKhanhHoang_DOTNET_MVC.Service.Helper;
using NguyenHuuKhanhHoang_DOTNET_MVC.Core.ValueObject.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NguyenHuuKhanhHoang_DOTNET_MVC.Core.Domain.Entity.Users;
using NguyenHuuKhanhHoang_DOTNET_MVC.Data;
using Microsoft.EntityFrameworkCore;
using System.Security.Cryptography;

namespace NguyenHuuKhanhHoang_DOTNET_MVC.Service.Service.Users
{
    public class UserService: IUserService
    {
        IRepository<User> _usersRepository;
        IRepository<UserRole> _userRolesRepository;
        public UserService(IRepository<User> usersRepository, IRepository<UserRole> userRolesRepository) {
            _usersRepository = usersRepository;
            _userRolesRepository = userRolesRepository;
        }

        public UserVO GetDeatilUser(string name , string pass)
        {
            var userLst = GenerateDataHelper.DummyUser();
            var result = userLst.Where(x => x.Name.Equals(name) && x.Password.Equals(pass)).FirstOrDefault();
            return result;
        }
        public User GetDeatilUserLogin1(string name, string pass)
        {

            var result = _usersRepository.TableNoTracking.Include(x => x.UserRoles).ThenInclude(x => x.Role).Where(x => x.Name.Equals(name) && x.Password.Equals(pass)).FirstOrDefault();


            return result;
        }
        public User GetDeatilUserLogin(string name, string pass)
        {
            var result = _usersRepository.TableNoTracking.Include(x => x.UserRoles).ThenInclude(x => x.Role).Where(x => x.Name.Equals(name)).FirstOrDefault();
            if(result == null)
            {
                return result;
            }
             var check = CheckPassWord(result.Password, pass);
            if (check == false)
            {
                return null;
            }
            return result;
        }
        private bool CheckPassWord(string passHash , string password)
        {
            /* Fetch the stored value */
            string savedPasswordHash = passHash;
            /* Extract the bytes */
            byte[] hashBytes = Convert.FromBase64String(savedPasswordHash);
            /* Get the salt */
            byte[] salt = new byte[16];
            Array.Copy(hashBytes, 0, salt, 0, 16);
            /* Compute the hash on the password the user entered */
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 10000);
            byte[] hash = pbkdf2.GetBytes(20);
            /* Compare the results */
            for (int i = 0; i < 20; i++)
                if (hashBytes[i + 16] != hash[i])
                    return false;
            return true;
        }
        public User CheckUserExist(string name)
        {

            var result = _usersRepository.TableNoTracking.Where(x => x.Name.Equals(name)).FirstOrDefault();
            return result;
        }
        public string PasswordHash(string password)
        {
            byte[] salt;
            new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 10000);
            byte[] hash = pbkdf2.GetBytes(20);
            byte[] hashBytes = new byte[36];
            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hash, 0, hashBytes, 16, 20);
            string savedPasswordHash = Convert.ToBase64String(hashBytes);
            return savedPasswordHash;
        }
        public void AddNewUser(User userEntity)
        {
            _usersRepository.Add(userEntity);
        }
        public void AddNewRole(UserRole userRoleEntity)
        {
            _userRolesRepository.Add(userRoleEntity);
        }
    }
}
