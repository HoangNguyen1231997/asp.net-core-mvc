﻿using NguyenHuuKhanhHoang_DOTNET_MVC.Core.Domain.Entity.Comments;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DOTNET_MVC.Service.Service.News
{
    public interface INewsService
    {
        void UpdateNews(Core.Domain.Entity.News.News newsEntity);
        Core.Domain.Entity.News.News GetNewsById(long id);
        void AddComment(Comment commentEntity);
        Core.ValueObject.News.NewsVO GetNewsDetail(long id);
        Task<List<Core.Domain.Entity.News.News>> GetNewList(int typeNew);
        Task<Core.Domain.Entity.News.News> GetNewsDetailValue(long id);
        void AddNewNews(Core.Domain.Entity.News.News newsEntity);
        void DeleteNews(long id);
        Task<List<Core.Domain.Entity.News.News>> GetNewListByUser(long userId);
    }
}
