﻿using Microsoft.EntityFrameworkCore;
using NguyenHuuKhanhHoang_DOTNET_MVC.Core.Domain.Entity.Comments;
using NguyenHuuKhanhHoang_DOTNET_MVC.Data;
using NguyenHuuKhanhHoang_DOTNET_MVC.Service.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DOTNET_MVC.Service.Service.News
{
    public class NewsService: INewsService
    {
        IRepository<Core.Domain.Entity.News.News> _newsRepository;
        IRepository<Comment> _commentRepository;
        public NewsService(IRepository<Core.Domain.Entity.News.News> newsRepository, IRepository<Comment> commentRepository) 
        {
            _commentRepository = commentRepository;
            _newsRepository = newsRepository;
        }

        public List<Core.ValueObject.News.NewsVO> GetNew(int typeNew)
        {

            var newsLst = GenerateDataHelper.DummyData();
            var result =newsLst.Where(x => x.Type.Equals(typeNew)).ToList();
            return result;
        }
        public async Task<List<Core.Domain.Entity.News.News>> GetNewList(int typeNew)
        {
            var result = await _newsRepository.TableNoTracking.Include(x=>x.NewsDetail).Where(x => x.Type.Equals(typeNew)).ToListAsync();
            return result;
        }
        public Core.Domain.Entity.News.News GetNewsById(long id)
        {
            var result = _newsRepository.TableNoTracking.Include(x=>x.NewsDetail).Where(x => x.Id.Equals(id)).FirstOrDefault();
            return result;
        }
        public async Task<List<Core.Domain.Entity.News.News>> GetNewListByUser(long userId)
        {
            var result = await _newsRepository.TableNoTracking.Include(x => x.NewsDetail).Where(x => x.UserId.Equals(userId)).ToListAsync();
            return result;
        }
        public void  AddNewNews(Core.Domain.Entity.News.News newsEntity)
        {
            _newsRepository.Add(newsEntity);
        }
        public void UpdateNews(Core.Domain.Entity.News.News newsEntity)
        {
            _newsRepository.Update(newsEntity);
        }
        public void DeleteNews(long id)
        {
            var entity = _newsRepository.Table.Include(x=>x.NewsDetail).Include(x=>x.Comments).Where(x=>x.Id.Equals(id)).FirstOrDefault();
            _newsRepository.Delete(entity);
        }
        public void AddComment(Comment commentEntity)
        {
            _commentRepository.Add(commentEntity);
        }
        public async Task<Core.Domain.Entity.News.News> GetNewsDetailValue(long id)
        {
            var result = await _newsRepository.TableNoTracking.Include(x => x.NewsDetail).Include(x=>x.User).Include(x=>x.Comments).Where(x => x.Id.Equals(id)).FirstOrDefaultAsync();
           
            return result;
        }
        public Core.ValueObject.News.NewsVO GetNewsDetail(long id)
        {
            var newsLst = GenerateDataHelper.DummyData();
            var result = newsLst.Where(x => x.Id.Equals(id)).FirstOrDefault();
            return result;
        }
    }
}
