﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DOTNET_MVC.Models
{
    public class LoginViewModel
    {
        [Display(Name="Tên đăng nhập")]
        [Required(ErrorMessage = "Tên đăng nhập yêu cầu nhập")]
        public string UserName { get; set; }
        [Display(Name = "Mật khẩu")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage ="Mật khẩu yêu cầu nhập")]
        public string Password { get; set; }
    }
}
