﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DOTNET_MVC.Models
{
    public class CommentViewModel
    {
        public string NoiDung { get; set; }
        public long? UserId { get; set; }
        public long? NewsId { get; set; }
    }
}
