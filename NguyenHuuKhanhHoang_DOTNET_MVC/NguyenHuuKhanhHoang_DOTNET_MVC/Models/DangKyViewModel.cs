﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DOTNET_MVC.Models
{
    public class DangKyViewModel
    {
        [Display(Name = "Tên đăng nhập")]
        [Required(ErrorMessage = "Tên đăng nhập yêu cầu nhập")]
        public string UserName { get; set; }
        [Display(Name = "Mật khẩu")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Mật khẩu yêu cầu nhập")]
        public string Password { get; set; }
        [Display(Name = "Xác nhận mật khẩu")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Xác nhận mật khẩu yêu cầu nhập")]
        [Compare("Password",ErrorMessage ="Mật khẩu không trùng khớp")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Giới thiệu")]
        [Required(ErrorMessage = "Giới thiệu yêu cầu nhập")]
        public string GioiThieu { get; set; }
    }
}
