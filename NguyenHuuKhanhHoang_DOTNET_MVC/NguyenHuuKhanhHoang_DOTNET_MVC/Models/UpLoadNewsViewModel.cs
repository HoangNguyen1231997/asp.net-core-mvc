﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DOTNET_MVC.Models
{
    public class UpLoadNewsViewModel
    {
        [Display(Name = "Tiêu Đề")]
        [Required(ErrorMessage = "Tiêu đề yêu cầu nhập")]
        public string TieuDe { get; set; }
        public long? UserId { get; set; }
        [Display(Name = "Thể loại")]
        [Required(ErrorMessage = "Thể loại yêu cầu nhập")]
        public string TheLoai { get; set; }
        public int? Type { get; set; }
        [Required(ErrorMessage = "Hình ảnh chính yêu cầu nhập")]
        public  IFormFile? File { get; set; }
        [Required(ErrorMessage = "Hình ảnh phụ yêu cầu nhập")]
        public IFormFile? FilePhu { get; set; }
        [Display(Name = "Nội dung")]
        [Required(ErrorMessage = "Nội dung yêu cầu nhập")]
        public string NoiDung { get; set; }
        [Display(Name = "Nội dung phụ")]
        [Required(ErrorMessage = "Nội dung phụ yêu cầu nhập")]
        public string NoiDungSub { get; set; }
        public string ThoiGianDang { get; set; }
        public string ImageUrl { get; set; }
        public string ImageUrlSub { get; set; }
    }
    public class UpDateNewsViewModel
    {
        public long? Id { get; set; }
        public long? NewDetailId { get; set; }
        [Display(Name = "Tiêu Đề")]
        [Required(ErrorMessage = "Tiêu đề yêu cầu nhập")]
        public string TieuDe { get; set; }
        public long? UserId { get; set; }
        [Display(Name = "Thể loại")]
        [Required(ErrorMessage = "Thể loại yêu cầu nhập")]
        public string TheLoai { get; set; }
        public int? Type { get; set; }
        
        public IFormFile? File { get; set; }
       
        public IFormFile? FilePhu { get; set; }
        [Display(Name = "Nội dung")]
        [Required(ErrorMessage = "Nội dung yêu cầu nhập")]
        public string NoiDung { get; set; }
        [Display(Name = "Nội dung phụ")]
        [Required(ErrorMessage = "Nội dung phụ yêu cầu nhập")]
        public string NoiDungSub { get; set; }
        public string ThoiGianDang { get; set; }
        public string ImageUrl { get; set; }
        public string ImageUrlSub { get; set; }
    }
}
