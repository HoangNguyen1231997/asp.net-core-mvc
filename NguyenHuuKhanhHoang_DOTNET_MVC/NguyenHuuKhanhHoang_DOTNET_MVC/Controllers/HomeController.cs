﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NguyenHuuKhanhHoang_DOTNET_MVC.Core.Domain.Entity.News;
using NguyenHuuKhanhHoang_DOTNET_MVC.Models;
using NguyenHuuKhanhHoang_DOTNET_MVC.Service.Service.News;
using NguyenHuuKhanhHoang_DOTNET_MVC.Service.Service.Users;

namespace NguyenHuuKhanhHoang_DOTNET_MVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private IHostingEnvironment _hostingEnviroment; 
        private INewsService _newsService;
        private IUserService _usersService;
        public HomeController(ILogger<HomeController> logger, INewsService newsService , IHostingEnvironment hostingEnviroment, IUserService usersService)
        {
            _newsService = newsService;
            _logger = logger;
            _hostingEnviroment = hostingEnviroment;
            _usersService = usersService;
        }
        
        public async Task<IActionResult> Index(int page=1)
        {
            
            int valueTake = 4;
            int skipValue = Convert.ToInt32((valueTake * page) - valueTake);
            var result = await _newsService.GetNewList(2);
            int pageCount = result.Count/valueTake;
            if (result.Count % valueTake > 0)
            {
                pageCount++;
            }
             result = result.Skip(skipValue).Take(valueTake).ToList();
            var request = HttpContext.User.Claims.ToList();
            if(request.Count > 0)
            {
                ViewBag.UserName = request[0].Value;
            }
            ViewBag.PageNumber = page;
            ViewBag.PageCount = pageCount;
            return View(result);
        }
        public IActionResult AccessDenied()
        {
            var request = HttpContext.User.Claims.ToList();
            if (request.Count > 0)
            {
                ViewBag.UserName = request[0].Value;
            }
            return View();
        }
        public IActionResult DeleteNew(long id)
        {
            var request = HttpContext.User.Claims.ToList();
            long userId = 0;
            if (request.Count > 0)
            {
                ViewBag.UserName = request[0].Value;
                userId = Convert.ToInt64(request[2].Value);
            }
            _newsService.DeleteNews(id);
            return RedirectToAction("UserInformation", new { id = userId });
        }
        public async Task<IActionResult> UserInformation(int page = 1)
        {
            long userId = 0;
            var request = HttpContext.User.Claims.ToList();
            if (request.Count > 0)
            {
                 userId = Convert.ToInt64(request[2].Value);
                ViewBag.UserName = request[0].Value;
                var user = _usersService.CheckUserExist(request[0].Value);
                ViewBag.UserInformation = user;
            }
            int valueTake = 4;
            int skipValue = Convert.ToInt32((valueTake * page) - valueTake);
            var result = await _newsService.GetNewListByUser(userId);
            int pageCount = result.Count / valueTake;
            if (result.Count % valueTake > 0)
            {
                pageCount++;
            }
            result = result.Skip(skipValue).Take(valueTake).ToList();
            ViewBag.PageNumber = page;
            ViewBag.PageCount = pageCount;
            return View(result);
        }
        [HttpPost]
        [Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme, Roles = "Admin")]
        public IActionResult UpLoadData(UpLoadNewsViewModel upLoadViewModel)
        {
            long userId = 0;
            var request = HttpContext.User.Claims.ToList();
            if (request.Count > 0)
            {
                userId = Convert.ToInt64(request[2].Value);
            }
            string nameOfFileChinh = null;
            string nameOfFilePhu = null;
            if (ModelState.IsValid)
            {
                string uploadFolder = Path.Combine(_hostingEnviroment.WebRootPath, "images");
                nameOfFileChinh = Guid.NewGuid().ToString() + "_" + upLoadViewModel.File.FileName;
                string filePath = Path.Combine(uploadFolder, nameOfFileChinh);
                upLoadViewModel.File.CopyTo(new FileStream(filePath, FileMode.Create));
                nameOfFilePhu = Guid.NewGuid().ToString() + "_phu_" + upLoadViewModel.FilePhu.FileName;
                string filePathPhu = Path.Combine(uploadFolder, nameOfFilePhu);
                upLoadViewModel.FilePhu.CopyTo(new FileStream(filePathPhu, FileMode.Create));

                var newsEntity = new News()
                {
                    TieuDe = upLoadViewModel.TieuDe,
                    TheLoai = upLoadViewModel.TheLoai,
                    Type = 2,
                    UserId = userId,
                    NewsDetail = new NewsDetail()
                    {
                        ImageUrl = "/images/" + nameOfFileChinh,
                        ImageUrlSub = "/images/" + nameOfFilePhu,
                        ThoiGianDang = DateTime.Now.ToString("MMM dd yyyy"),
                        NoiDung = upLoadViewModel.NoiDung,
                        NoiDungSub = upLoadViewModel.NoiDungSub,
                    }
                };
                _newsService.AddNewNews(newsEntity);
            }
            else
            {
                ViewBag.UserName = request[0].Value;
                return View("UploadNews", upLoadViewModel);
            }


            return RedirectToAction("Index", "Home");
        }
        [Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme, Roles = "Admin")]
        public IActionResult UploadNews()
        {
            var request = HttpContext.User.Claims.ToList();
            if (request.Count > 0)
            {
                ViewBag.UserName = request[0].Value;
            }
            return View();
        }
        public IActionResult UpdateNews(long id)
        {
           
            var request = HttpContext.User.Claims.ToList();
            if (request.Count > 0)
            {
                ViewBag.UserName = request[0].Value;
            }
            var result = _newsService.GetNewsById(id);
            var newsUpdate = new UpDateNewsViewModel()
            {
                Id = result.Id,
                NewDetailId = result.NewsDetail.Id,
                NoiDung= result.NewsDetail.NoiDung,
                NoiDungSub = result.NewsDetail.NoiDungSub,
                ThoiGianDang = result.NewsDetail.ThoiGianDang,
                TheLoai = result.TheLoai,
                TieuDe = result.TieuDe,
                UserId = result.UserId,
                Type= result.Type,
                ImageUrl  = result.NewsDetail.ImageUrl,
                ImageUrlSub = result.NewsDetail.ImageUrlSub
            };

            return View(newsUpdate);
        }
        [HttpPost]
        [Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme, Roles = "Admin")]
        public IActionResult UpDateData(UpDateNewsViewModel upDateViewModel)
        {

            long userId = 0;
            var request = HttpContext.User.Claims.ToList();
            if (request.Count > 0)
            {
                userId = Convert.ToInt64(request[2].Value);
            }
           
            if (ModelState.IsValid)
            {
                string uploadFolder = Path.Combine(_hostingEnviroment.WebRootPath, "images");
                string filePath = String.Empty;
                string filePathPhu = String.Empty;
                string nameOfFileChinh = String.Empty;
                string nameOfFilePhu = String.Empty;

                if (upDateViewModel.File != null)
                {
                    nameOfFileChinh = Guid.NewGuid().ToString() + "_" + upDateViewModel.File.FileName;
                    filePath = Path.Combine(uploadFolder, nameOfFileChinh);
                    upDateViewModel.File.CopyTo(new FileStream(filePath, FileMode.Create));
                }
                if (upDateViewModel.FilePhu != null)
                {
                    nameOfFilePhu = Guid.NewGuid().ToString() + "_phu_" + upDateViewModel.FilePhu.FileName;
                    filePathPhu = Path.Combine(uploadFolder, nameOfFilePhu);
                    upDateViewModel.FilePhu.CopyTo(new FileStream(filePathPhu, FileMode.Create));
                }
                 if(nameOfFileChinh == String.Empty)
                {
                    nameOfFileChinh = upDateViewModel.ImageUrl;
                }
                else
                {
                    nameOfFileChinh = "/images/" + nameOfFileChinh;
                }
                if (nameOfFilePhu == String.Empty)
                {
                    nameOfFilePhu = upDateViewModel.ImageUrlSub;
                }
                else
                {
                    nameOfFilePhu = "/images/" + nameOfFilePhu;
                }
                var newsEntity = new News()
                {
                    Id = Convert.ToInt64(upDateViewModel.Id),
                    TieuDe = upDateViewModel.TieuDe,
                    TheLoai = upDateViewModel.TheLoai,
                    Type = Convert.ToInt32(upDateViewModel.Type),
                    UserId = userId,
                    NewsDetail = new NewsDetail()
                    {
                        Id = Convert.ToInt64(upDateViewModel.NewDetailId),
                        NewsId = Convert.ToInt64(upDateViewModel.Id),
                        ImageUrl = nameOfFileChinh,
                        ImageUrlSub = nameOfFilePhu,
                        ThoiGianDang = DateTime.Now.ToString("MMM dd yyyy"),
                        NoiDung = upDateViewModel.NoiDung,
                        NoiDungSub = upDateViewModel.NoiDungSub,
                    }
                };
                    _newsService.UpdateNews(newsEntity);
            }
            else
            {
                ViewBag.UserName = request[0].Value;
                return View("UpdateNews",upDateViewModel);
            }


            return RedirectToAction("UserInformation", "Home");
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
