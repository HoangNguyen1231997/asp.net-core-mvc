﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NguyenHuuKhanhHoang_DOTNET_MVC.Core.Domain.Entity.Comments;
using NguyenHuuKhanhHoang_DOTNET_MVC.Models;
using NguyenHuuKhanhHoang_DOTNET_MVC.Service.Service.News;

namespace NguyenHuuKhanhHoang_DOTNET_MVC.Controllers
{
    public class NewsDetailController : Controller
    {
        INewsService _newsService;
        public NewsDetailController(INewsService newsService)
        {
            _newsService = newsService;
        }
        
        [HttpPost]
        public IActionResult CommentNews(CommentViewModel commentViewModel)
        {
            var request = HttpContext.User.Claims.ToList();
            var a = ViewBag.NewsId;
            string userName = String.Empty;
            long userId = 0;
            if (request.Count > 0)
            {
                ViewBag.UserName = request[0].Value;
                userName= request[0].Value;
                userId = Convert.ToInt64(request[2].Value);
            }
            var comment = new Comment()
            {
                NewsId = Convert.ToInt64(commentViewModel.NewsId),
                NoiDung = commentViewModel.NoiDung,
                UserId = userId,
                UserName = userName
            };
            _newsService.AddComment(comment);
            return RedirectToAction("Index",new {id = commentViewModel.NewsId });
        }
        [Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Index(long id)
        {
            ViewBag.NewsId = id;
            var request = HttpContext.User.Claims.ToList();
            if (request.Count > 0)
            {
                ViewBag.UserName = request[0].Value;
            }
            var result = await _newsService.GetNewsDetailValue(id);
            var comment = new CommentViewModel();
            return View(comment);
        }
        
    }
}