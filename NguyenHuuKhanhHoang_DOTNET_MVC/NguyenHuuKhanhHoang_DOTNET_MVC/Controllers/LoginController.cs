﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using NguyenHuuKhanhHoang_DOTNET_MVC.Core.Domain.Entity.Users;
using NguyenHuuKhanhHoang_DOTNET_MVC.Models;
using NguyenHuuKhanhHoang_DOTNET_MVC.Service.Service.Users;

namespace NguyenHuuKhanhHoang_DOTNET_MVC.Controllers
{
    public class LoginController : Controller
    {
        private IUserService _userService;
        public LoginController(IUserService userService)
        {
            _userService = userService;
        }
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> Logout()
        {
            var request = HttpContext.User.Claims.ToList();
            await HttpContext.SignOutAsync(
                    scheme: CookieAuthenticationDefaults.AuthenticationScheme);

            return RedirectToAction("Index", "Login");
        }
        public IActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public IActionResult DangKy(DangKyViewModel dangkyViewModel)
        {
            if (ModelState.IsValid)
            {
                var userExist = _userService.CheckUserExist(dangkyViewModel.UserName);
                if(userExist == null)
                {
                    string passwordHashed = _userService.PasswordHash(dangkyViewModel.Password);
                    var user = new User()
                    {
                        Name = dangkyViewModel.UserName,
                        Password = passwordHashed,
                        GioiThieu = dangkyViewModel.GioiThieu
                    };
                    _userService.AddNewUser(user);
                    var getUser = _userService.CheckUserExist(dangkyViewModel.UserName);
                    var role = new UserRole()
                    {
                        UserId = getUser.Id,
                        RoleId = 2
                    };
                    _userService.AddNewRole(role);
                }
               
                else
                {
                    ModelState.AddModelError("", "Tài khoản đã tồn tại");
                    return View("Register",dangkyViewModel);
                }

            }
            else
            {
                return View("Register", dangkyViewModel);
            }



            return RedirectToAction("Index", "Login");
        }


        [HttpPost]
        public IActionResult Login(LoginViewModel loginViewModel)
        {
           
            if (ModelState.IsValid)
            {
                var user = _userService.GetDeatilUserLogin(loginViewModel.UserName, loginViewModel.Password);
                if(user != null)
                {
                    var claims = new List<Claim>
                 {
                     new Claim(ClaimTypes.Name, user.Name),
                     new Claim(ClaimTypes.Role,user.UserRoles.FirstOrDefault().Role.RoleName),
                     new Claim("_id", user.Id.ToString()),
                 };
                    var claimsIdentity = new ClaimsIdentity(
                            claims, CookieAuthenticationDefaults.AuthenticationScheme);
                    ClaimsPrincipal principal = new ClaimsPrincipal(claimsIdentity);
                    var prop = new AuthenticationProperties();
                    HttpContext.SignInAsync(
                   CookieAuthenticationDefaults.AuthenticationScheme,
                    principal: principal,
                    properties: prop).Wait();
                }
                else
                {
                    ModelState.AddModelError("", "Tài khoản hoặc mật khẩu không đúng");
                    return View("Index",loginViewModel);
                }

            }
            else
            {
                return View("Index", loginViewModel);
            }
            
            

            return RedirectToAction("Index","Home");
        }
    }
}