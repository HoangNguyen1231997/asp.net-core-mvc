﻿using Microsoft.AspNetCore.Mvc;
using NguyenHuuKhanhHoang_DOTNET_MVC.Service.Service.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DOTNET_MVC.HomeViewComponents
{
    public class DetailList : ViewComponent
    {
        INewsService _newsService;
        public DetailList(INewsService newsService)
        {
            _newsService = newsService;
        }
        public async Task<IViewComponentResult> InvokeAsync(long id)
        {
            ViewBag.CommentCount = 0;
            var result = await _newsService.GetNewsDetailValue(id);
            if(result != null)
            {
                ViewBag.CommentCount = result.Comments.Count > 0 ? result.Comments.Count() : 0;
              
            }
            
            return View(result);
        }
    }
}
