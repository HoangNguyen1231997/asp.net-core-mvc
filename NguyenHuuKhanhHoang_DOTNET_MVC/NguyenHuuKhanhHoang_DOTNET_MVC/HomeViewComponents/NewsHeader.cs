﻿using Microsoft.AspNetCore.Mvc;
using NguyenHuuKhanhHoang_DOTNET_MVC.Service.Service.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DOTNET_MVC.HomeViewComponents
{
    public class NewsHeader: ViewComponent
    {
        INewsService _newsService;
        public NewsHeader(INewsService newsService)
        {
            _newsService = newsService;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            
            var result = await _newsService.GetNewList(1);
            return View(result);
        }
    }
}
