﻿using Microsoft.AspNetCore.Mvc;
using NguyenHuuKhanhHoang_DOTNET_MVC.Service.Service.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DOTNET_MVC.HomeViewComponents
{
    public class NewsRight : ViewComponent
    {
        INewsService _newsService;
        public NewsRight(INewsService newsService)
        {
            _newsService = newsService;
        }
        public IViewComponentResult Invoke()
        {
            return View();
        }
    }
}
