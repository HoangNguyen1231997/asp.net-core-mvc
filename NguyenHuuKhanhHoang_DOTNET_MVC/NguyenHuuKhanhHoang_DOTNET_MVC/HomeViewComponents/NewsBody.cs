﻿using Microsoft.AspNetCore.Mvc;
using NguyenHuuKhanhHoang_DOTNET_MVC.Service.Service.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DOTNET_MVC.HomeViewComponents
{
    public class NewsBody : ViewComponent
    {
        INewsService _newsService;
        public NewsBody(INewsService newsService)
        {
            _newsService = newsService;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {

            var result = await _newsService.GetNewList(2);
            return View(result);
        }
    }
}
