﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DOTNET_MVC.Data
{
    public class EFRepository<TEntity> :  IRepository<TEntity> where TEntity : class
    {
        private readonly NashTechDbContext _context;
        private DbSet<TEntity> _entities;
        public EFRepository(NashTechDbContext context)
        {
            _context = context;
        }

        public virtual DbContext Context => _context;

        public IQueryable<TEntity> Table => Entities;

        public IQueryable<TEntity> TableNoTracking => Entities.AsNoTracking();
        protected virtual DbSet<TEntity> Entities
        {
            get
            {
                if (_entities == null)
                    _entities = _context.Set<TEntity>();

                return _entities;
            }
        }
        #region ADD DELETE UPDATE
        public void Add(TEntity entity)
        {
            if (entity == null)
                throw new NotImplementedException();

            Entities.Add(entity);
            _context.SaveChanges();
        }

        

        public void AddRange(IEnumerable<TEntity> entities)
        {
            if (entities == null)
                throw new NotImplementedException();

            Entities.AddRange(entities);
            _context.SaveChanges();
        }

        

        public void Delete(TEntity entity)
        {
            if (entity == null)
                throw new NotImplementedException();

            Entities.Remove(entity);
            _context.SaveChanges();
        }
        public void Delete(IEnumerable<TEntity> entities)
        {
            if (entities == null)
                throw new NotImplementedException();

            Entities.RemoveRange(entities);
            _context.SaveChanges();
        }

        

        public void Update(TEntity entity)
        {
            if (entity == null)
                throw new NotImplementedException();

            Entities.Update(entity);
            _context.SaveChanges();

        }

        public void Update(IEnumerable<TEntity> entities)
        {
            if (entities == null)
                throw new NotImplementedException();

            Entities.UpdateRange(entities);
            _context.SaveChanges();
        }


        #endregion
       
    }
}
